create TABLE riporting (
run_id FLOAT,
vallalat_cd float,
up_id float, 
bekotes_id float,
szfszla_id float,
eltero_szf_id float,
bf_cd VARCHAR,
szerzodes_id int,
bekoltozes_dt VARCHAR,
kikoltozes_dt VARCHAR,
szamlazo_szolg_cd varchar,
fogyhely_id float,
bekotesfajta_cd float,
bekotesfajta_nm varchar,
csatlakozasi_obj_id int,
fogyhely_fajta_cd float,
fogyhely_fajta_nm varchar,
agazat_cd VARCHAR,
elszo_cd varchar,
tarifatipus_cd varchar,
valid_from_dt varchar,
valid_to_dt varchar,
cimszam_id float,
telepules_id float,
utca_id float,
hazszam_cd varchar
);


CREATE INDEX riporting_index ON riporting (up_id);

COPY riporting FROM 'C:\riporting_torzs_c.csv'  DELIMITERS ',' HEADER CSV ENCODING 'ISO-8859-1';

SELECT count(*) from riporting;
select * from riporting;

