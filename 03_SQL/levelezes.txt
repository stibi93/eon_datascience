create TABLE levelezes (
run_id FLOAT,
vallalat_cd float,
up_id float,
levelezes_fajta int,
kiall_dat date
);


CREATE INDEX levelezes_index ON levelezes (up_id);

COPY levelezes FROM 'C:\levelezes_c.csv'  DELIMITERS ',' HEADER CSV ENCODING 'ISO-8859-1';

SELECT count(*) from levelezes;
select * from levelezes;

